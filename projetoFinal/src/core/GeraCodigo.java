package core;

import java.util.LinkedList;
import java.util.List;

import core.generator.Template;

public class GeraCodigo {
	
	private static GeraCodigo instance; 
	private List<String> lObject = new LinkedList<String>();
	private Template template = Template.getInstance();
	
	private GeraCodigo() {
		
	}

	public static GeraCodigo getInstance() {
		if(instance == null) {
			instance = new GeraCodigo();
		}
		return instance;
	}
	
	public void addElement(String elem) {
		if(!lObject.contains("=") )  {
			lObject.add(elem);
		}
	}
	public void showList() {
		String out = "LISTA: ";
		int go = 0;
		int back = lObject.size()-1;
		for (int i = 0; i < lObject.size(); i++) {
			if(i%2 == 0) {
				out += lObject.get(go) + " ";
				go++;
			}
			else {
				out += lObject.get(back) + " ";
				back--;
			}
			
			
		}
		
		String[] code = out.split(" ");
		System.out.println("##############CODIGO GERADO #################");
		if(code.length > 4) {
			if(code[4].equals("+")) {
				String tem = template.createOperationTemplate(code[1],code[3], code[5], "ADD");
				System.out.println(tem);
			}
			else if(code[4].equals("-")) {
				String tem = template.createOperationTemplate(code[1],code[3], code[5], "SUB");
				System.out.println(tem);
			}
			else if(code[4].equals("*")) {
				String tem = template.createOperationTemplate(code[1],code[3], code[5], "MULT");
				System.out.println(tem);
			}
			else if(code[4].equals("/")) {
				String tem = template.createOperationTemplate(code[1],code[3], code[5], "DIV");
				System.out.println(tem);
			}
			
		}
		else if(code.length <= 4) {
			String tem = template.createAttrTemplate(code[1],code[3]);
			System.out.println(tem);
		}
		System.out.println("#############################################");
	}
	
	public  void genCode() {
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < lObject.size(); i++) {
			sb.append(lObject.get(i));
		}
		if(sb.toString().contains("RETURN")) {
			String out = template.createCollection(lObject);
			System.out.println(out);
		}
		else {
			System.out.println(sb.toString());
			String out = template.createTempCode(lObject);
			System.out.println(out);
		}
	}
	
}
