package core;

import java.util.ArrayList;
import java.util.List;

import core.modeling.GerenciadorEntidades;
import core.modeling.metamodel.Atributo;
import core.modeling.metamodel.Classe;
import core.modeling.metamodel.Metodo;
import core.modeling.metamodel.Pacote;
import core.modeling.parser.XMIParserException;
import core.nodos.Node;

public class AnaliseSemanticaOCL {

	private static AnaliseSemanticaOCL instance;
	private static Node nodeInstance;
	private String context;
	private static String xmiFileName = "src/Test/xmi_projeto.xml";;
	private static GerenciadorEntidades gerenciadorEntidades;
	private Contexto contexto;
	private static boolean collection = false;
	private static boolean expCollectionOK = false;
	private static List<String> operationCollectList = new ArrayList<String>();
	private static List<String> operationOPList = new ArrayList<String>();
	public static Object operacaoAtual = "";
	public static Object symb = "";

	private AnaliseSemanticaOCL() {

	}

	public static AnaliseSemanticaOCL getInstance() {
		if (instance == null) {
			insertOperations();
			insertOpCollec();
			instance = new AnaliseSemanticaOCL();
			gerenciadorEntidades = GerenciadorEntidades.getInstance();
			try {
				gerenciadorEntidades.read(xmiFileName);
			} catch (XMIParserException e) {
				e.printStackTrace();
			}
		}
		return instance;
	}

	

	public String getContext() {
		return context;
	}

	public void setContext(String context) {
		this.context = context;
	}

	public Node getNodeInstance() {
		if (nodeInstance == null) {
			nodeInstance = new Node("Root", "Root");
		}
		return nodeInstance;
	}

	public void setClassOnContex() {
		Classe classe = GerenciadorEntidades.getInstance().getClasse(
				contexto.getClasseOuPacote());

		contexto.setClasse(classe);
	}

	public void contextStart() {
		contexto = new Contexto();
	}

	public void turnOnCollection() {
		setCollection(true);
	}

	public void setCurrentClass(Object o) {
		contexto.setClasseOuPacote(String.valueOf(o));
	}

	public void checkClassType(int linha, int coluna) {
		String nomeClasse = contexto.getClasseOuPacote();

		Pacote pacoteDefault = GerenciadorEntidades.getInstance().getPacotes()
				.getPacotePadrao();

		if (!pacoteDefault.temClasse(nomeClasse)) {
			throw new AnaliseSemanticaException("A classe '" + nomeClasse
					+ "' nao existe no pacote default!", linha, coluna);
		}
	}

	public void checkOperation(int linha, int coluna, Object nome) {
		String nomeOp = String.valueOf(nome);

		Classe classeContexto = contexto.getClasse();

		if (!classeContexto.temMetodo(nomeOp)) {
			throw new AnaliseSemanticaException("A classe '"
					+ contexto.getClasse().getNome()
					+ "' nao tem uma operacao com nome '" + nome + "'!", linha,
					coluna);
		}
	}

	public void checkReturnType(int linha, int coluna, Object tipo) {
		String tipoRetorno = String.valueOf(tipo);

		Metodo metodoContexto = contexto.getMetodo();
		String tipoRetornoEsperado = metodoContexto.getTipoRetorno().getNome();

		if (!tipoRetorno.equals(tipoRetornoEsperado)) {
			throw new AnaliseSemanticaException(
					"Tipo de retorno invalido, esperava '"
							+ tipoRetornoEsperado + "' e veio '" + tipoRetorno
							+ "'!", linha, coluna);
		}
	}

	public void checkParamNum(int linha, int coluna) {
		Metodo metodoContexto = contexto.getMetodo();

		int quantidadeDeParametrosEsperada = metodoContexto.getParametros()
				.size();
		if (quantidadeDeParametrosEsperada > contexto
				.getQuantidadeDeParametros()) {
			throw new AnaliseSemanticaException(
					"Quantidade de parametros para o metodo '"
							+ metodoContexto.getNome()
							+ "' incorreta, foram definidos menos parametros que o esperado!",
					linha, coluna);
		}
	}

	public void setCurrentMethod(Object o) {
		String nomeMetodo = String.valueOf(o);
		Classe classe = contexto.getClasse();
		Metodo metodo = classe.getMetodo(nomeMetodo);

		contexto.setMetodo(metodo);
	}

	public void addnewParam(Object nome, Object tipo) {
		String nomeParametro = String.valueOf(nome);

		Metodo m = contexto.getMetodo();

		int i = contexto.getQuantidadeDeParametros();
		Classe tipoProximoParametro = m.getParametros().get(i).getTipo();

		contexto.addParametro(nomeParametro, tipoProximoParametro);
	}

	public void checkNextParam(int linha, int coluna, Object tipo) {
		String tipoParametro = String.valueOf(tipo);

		Metodo metodoContexto = contexto.getMetodo();

		int i = contexto.getQuantidadeDeParametros();
		try {
			String tipoProximoParametro = metodoContexto.getParametros().get(i)
					.getTipo().getNome();
			if (!tipoParametro.equals(tipoProximoParametro)) {
				throw new AnaliseSemanticaException("Tipo do " + (i + 1)
						+ "o parametro errado, esperava '"
						+ tipoProximoParametro + "' e veio '" + tipoParametro
						+ "'!", linha, coluna);
			}
		} catch (IndexOutOfBoundsException e) {
			throw new AnaliseSemanticaException(
					"Quantidade de parametros para o metodo '"
							+ metodoContexto.getNome()
							+ "' incorreta, foram definidos mais parametros que o esperado!",
					linha, coluna);
		}
	}

	public boolean isCollection() {
		return collection;
	}

	public void setCollection(boolean collection) {
		AnaliseSemanticaOCL.collection = collection;
	}

	public void checkColletionOperation(int linha, int coluna,
			Object operation, Object nameCollection) {
			
		if (operation != null && this.isCollection()) {
			if (nameCollection != null) {
				operacaoAtual = operation;
				Classe classeContexto = contexto.getClasse();
				boolean existeAttr = false;
				for (Atributo attr : classeContexto.getAtributos()) {
					if (attr.getNome().equals(nameCollection.toString())) {
						if(attr.getTipo().getNome().contains("<")) {
							existeAttr = true;
							break;
						}
						else {
							throw new AnaliseSemanticaException("Atributo "
									+ nameCollection.toString() + " n�o � uma cole��o",
									linha, coluna);
						}
					}
				}
				if (!existeAttr) {
					throw new AnaliseSemanticaException("Atributo "
					+ nameCollection.toString() + " n�o existe no Contexto",
					linha, coluna);
				}

			}
			if (!operationCollectList.contains(operation.toString())) {
				throw new AnaliseSemanticaException(
						"Tipo de acesso a cole��o n�o definido.", linha, coluna);
			}
			checkExpOp(linha, coluna);
		}
	}
	
	public void checkExpOp( int linha, int coluna) {
		/*if(String.valueOf(operacaoAtual).equals("collect")) {
			if (!operationOPList.contains(symb.toString())) {
				throw new AnaliseSemanticaException(
						"O operador de colecoes collect exige um resultado booleano", linha, coluna);
			}
		}*/
		
	}
	
	private static void insertOpCollec() {
		operationOPList.add(">");
		operationOPList.add(">=");
		operationOPList.add("<");
		operationOPList.add("<=");
		operationOPList.add("<>");
		operationOPList.add("and");
		operationOPList.add("or");
		operationOPList.add("xor");
		operationOPList.add("=");
		operationOPList.add("<>");
	}

	private static void insertOperations() {
		operationCollectList.add("Count");
		operationCollectList.add("execludes");
		operationCollectList.add("excludesAll");
		operationCollectList.add("includes");
		operationCollectList.add("includesAll");
		operationCollectList.add("isEmpty");
		operationCollectList.add("notEmpty");
		operationCollectList.add("size");
		operationCollectList.add("sum");
		operationCollectList.add("append");
		operationCollectList.add("asBag");
		operationCollectList.add("asOrderedSet");
		operationCollectList.add("asSequence");
		operationCollectList.add("asSet");
		operationCollectList.add("at");
		operationCollectList.add("excluding");
		operationCollectList.add("first");
		operationCollectList.add("flatten");
		operationCollectList.add("including");
		operationCollectList.add("indexOf");
		operationCollectList.add("insertAt");
		operationCollectList.add("intersection");
		operationCollectList.add("last");
		operationCollectList.add("prepend");
		operationCollectList.add("subOrderedSet");
		operationCollectList.add("subsequence");
		operationCollectList.add("symmetricDifference");
		operationCollectList.add("union");
		operationCollectList.add("collect");
		operationCollectList.add("collectNested");
		operationCollectList.add("exists");
		operationCollectList.add("forAll");
		operationCollectList.add("isUnique");
		operationCollectList.add("iterate");
		operationCollectList.add("one");
		operationCollectList.add("reject");
		operationCollectList.add("select");
		operationCollectList.add("sortedBy");
	}

	public static boolean isExpCollectionOK() {
		return expCollectionOK;
	}

	public static void setExpCollectionOK(boolean expCollectionOK) {
		AnaliseSemanticaOCL.expCollectionOK = expCollectionOK;
	}
}
