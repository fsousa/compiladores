package core;


public class Codigo {

	private String codigo;

	public Codigo(String codigo) {
		this.codigo = codigo;
	}

	public Codigo() {
		codigo = new String("");
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String toString() {
		return codigo.toString();
	}
}
