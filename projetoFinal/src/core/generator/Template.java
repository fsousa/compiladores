package core.generator;

import java.util.List;

public class Template {

	private int register = 0;
	private int label = 100;
	private int nReg = 1;
	public static Template instance;

	private Template() {

	}

	public static Template getInstance() {
		if (instance == null) {
			instance = new Template();
		}
		return instance;
	}

	public String createOperationTemplate(String a, String b, String c,
			String tipo) {
		int r0 = nextRegister();
		int r1 = nextRegister();
		String out = String.format(
				"%s: LD R%s, %s\n%s: LD R%s, %s\n%s: %s R%s, R%s, R%s\n%s: ST %s, R%s",nextLabel(), r0, b,nextLabel(),
				r1, c, nextLabel(), tipo, r0, r0, r1,nextLabel(), a, r0);
		return out;

	}

	public String createAttrTemplate(String a, String value) {
		int r0 = nextRegister();
		String out = String.format("%s: LD R%s, %s\n%s: ST %s, %s",
				nextLabel(), r0,value, nextLabel(), a, r0);
		return out;
	}

	public  String createFor(String i, List<String> lista) {
		int r0 = nextRegister();
		int r1 = nextRegister();
		int r2 = nextRegister();
		int label;
		String v = lista.get(3);

		String out = "";

	
		out = String
				.format("%s: LD R%s, %s\n%s: LD R%s, %s\n%s: %s R%s, R%s, R%s\n%s\n%s: INC R%s\n%s: BLTZ R%s, %s", nextLabel(),
						r0, i, nextLabel(), r1, v+".length", label = nextLabel(),"SUB", r2, r0, r1,createBody(lista), nextLabel(), r0, nextLabel(), r2, label);

		return out;
	}
	
	public String createBody(List<String> lista) {
		String x = lista.get(0);
		String y = lista.get(3);
		
		int r0 = nextRegister();
		int r1 = nextRegister();
		String out = String.format("%s: LD R%s, i\n%s: MUL R%s, R%s, 8\n%s: LD R%s, %s(R%s)\n%s: ST %s(R%s), R%s", nextLabel()
				,r0, nextLabel(), r0, r0, nextLabel(), r1, x, r0, nextLabel(), y, r0, r1 );
		return out;
		
	}
	
	public String createTempCode(List<String> lista) {
		String out = "";
		
		/*while (lista.get((int)Math.floor(lista.size()/2)).equals("*") ) {
			String pop = lista.remove((int)Math.floor(lista.size()/2));
			lista.add((int)Math.floor(lista.size())-1, pop);
		}*/
		
		for (int i = 0; i < lista.size(); i++) {
			if(lista.get(i).equals("*")) {
				String pop = lista.remove(i);
				lista.add(lista.size()-1, pop);
			}
		}
		
		for (int i = 0; i < Math.floor(lista.size()/2); i++) {
			int reg = nextRegister();
			out += String.format("%s: LD R%s, %s\n", nextLabel(), reg, lista.get(i+1));
		}
		for (int i = lista.size() -1 ; i >= (int)(Math.floor(lista.size()/2) + 1) ; i--) {
			if(lista.get(i).equals("+")) {
				out += String.format("%s: ADD R0, R0, R%s\n", nextLabel(),nextOpRegister());
			}
			else if(lista.get(i).equals("-")) {
				out += String.format("%s: SUB R0, R0, R%s\n", nextLabel(),nextOpRegister());
			}
			else if(lista.get(i).equals("*")) {
				out += String.format("%s: MUL R0, R0, R%s\n", nextLabel(),nextOpRegister());
			}
 		}
		out += String.format("%s: ST %s, R0\n", nextLabel(),lista.get(0));
		return  out;
	}

	public int nextRegister() {
		int out = register;
		register++;
		return out;
	}

	public int nextLabel() {
		int out = label;
		label++;
		return out;
	}
	
	public int nextOpRegister() {
		int out = nReg;
		nReg++;
		return out;
	}

	public String createCollection(List<String> lObject) {
		String out = createFor("i", lObject);
		return out;
	}

}
