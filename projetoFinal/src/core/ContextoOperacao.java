package core;

import java.util.ArrayList;

import core.modeling.metamodel.Classe;
import core.modeling.metamodel.Colecao;

public class ContextoOperacao {
	private String nomeOperacao;
	
	private Classe contextoAoIniciarOperacao;
	private boolean verificandoParametros;
	
	private ArrayList<Classe> tiposParametros;

	public ContextoOperacao(Classe contextoAoIniciarOperacao, String nomeOperacao) {
		this.tiposParametros = new ArrayList<Classe>();
		this.contextoAoIniciarOperacao = contextoAoIniciarOperacao;
		this.nomeOperacao = nomeOperacao;
	}

	public void addParameter(Classe tipo) {
		tiposParametros.add(tipo);
	}
	
	public ArrayList<Classe> getParametros(){
		return tiposParametros;
	}

	public void setVerificandoParametros(boolean verificandoParametros) {
		this.verificandoParametros = verificandoParametros;
	}

	public boolean isVerificandoParametros() {
		return verificandoParametros;
	}

	public Classe getContextoAoIniciarOperacao() {
		return contextoAoIniciarOperacao;
	}

	public boolean isOperacaoColecao() {
		return contextoAoIniciarOperacao != null && contextoAoIniciarOperacao.isColecao();
	}

	public boolean isOperacaoComFiltro() {
		if (!isOperacaoColecao()) {
			return false;
		}

		Colecao col = (Colecao) contextoAoIniciarOperacao;
		return col.getMetodo(nomeOperacao) != null
				&& col.getMetodo(nomeOperacao).isRecebeFiltro();
	}

	public void setNomeOperacao(String nomeOperacao) {
		this.nomeOperacao = nomeOperacao;
	}

	public String getNomeOperacao() {
		return nomeOperacao;
	}
}
