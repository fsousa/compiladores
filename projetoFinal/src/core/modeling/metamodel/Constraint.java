package core.modeling.metamodel;

import core.ConstantesAnaliseSemantica;

public class Constraint {
	private String stereotype;
	private String text;

	public Constraint() {
		this.text = "";
		this.stereotype = "";
	}

	public void setPreCondition() {
		this.stereotype = ConstantesAnaliseSemantica.PRE;
	}

	public void setPostCondition() {
		this.stereotype = ConstantesAnaliseSemantica.POST;
	}

	public String getStereotype() {
		return stereotype;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public void addText(String text) {
		this.text += text;
	}

}
