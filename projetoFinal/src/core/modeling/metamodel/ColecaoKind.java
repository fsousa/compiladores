package core.modeling.metamodel;

public enum ColecaoKind {
	BAG("Bag"), SET("Set"), ORDERED_SET("OrderedSet"), SEQUENCE("Sequence");

	private String nome;

	ColecaoKind(String nome) {
		this.nome = nome;
	}

	public String getNome() {
		return nome;
	}
}
