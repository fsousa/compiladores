package core.modeling.parser;

public interface XMIParserIF {
	public void parse() throws XMIParserException;
}
