package core;

public class AnaliseSemanticaException extends RuntimeException {
	private static final long serialVersionUID = 1L;

	public AnaliseSemanticaException(String mensagemDeErro, Integer linha,
			Integer coluna) {
		System.err.print(formataMensagemDeErro(mensagemDeErro, linha, coluna));
		System.exit(1);
	}

	private static String formataMensagemDeErro(String mensagemDeErro,
			Integer linha, Integer coluna) {
		String mensagem = "Erro semantico";

		if (linha != null && coluna != null) {
			mensagem += " na linha " + (linha + 1) + " e coluna "
					+ (coluna + 1);
		}

		return mensagem + ": " + mensagemDeErro;
	}
}
