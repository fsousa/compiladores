#! /bin/bash

for i in `ls Test`
do
	java -jar  jar/OCCompiler.jar Test/$i
done

for i in `ls ../src/TestError`
do
	echo "Testing " $i
	java -jar  jar/OCCompiler.jar TestError/$i | egrep "Number of errors = 0."
done
