import java_cup.runtime.*;

%%

%standalone
%class Lexer
%line
%column
%state STRING
%cup

%{
	private StringBuilder stringBuilder;
	
  	public boolean hasNext() {
  		return !zzAtEOF;
  	}
	
	private Symbol symbol(int type) {
    	return new Symbol(type, yyline, yycolumn);
  	}
  	
  	private Symbol symbol(int type, Object value) {
    	return new Symbol(type, yyline, yycolumn, value);
  	}
  	
  	private void error(String message) {
  		throw new RuntimeException("Erro Lexico: " + message);
  	}
  	public String current_lexeme(){
    int l = yyline+1;
    int c = yycolumn+1;
    return " (line: "+l+" , column: "+c+" , lexeme: '"+yytext()+"')";
  }
%}

boolean = "true" | "false"
numero = [0-9]+
letra = [a-zA-Z]
espacoEmBranco = [ \r\n\t\f]
inteiro = [-]?{numero}
pontoFlututante = {inteiro}\.{numero}([eE][+-]?[0-9]+)?
comentario = --([^\n\r])*[\n\r]
id = ({letra} | _)({letra} | {numero} | _)*
colecao = "Bag" | "Set" | "OrderedSet" | "Sequence"
delimString = \'

%%

<YYINITIAL> {comentario}    		{ }
<YYINITIAL> {espacoEmBranco}    	{ }
<YYINITIAL> "("						{ return symbol(sym.PARENTESQ, new String(yytext())); }
<YYINITIAL> ")"						{ return symbol(sym.PARENTDIR, new String(yytext())); }
<YYINITIAL> "."                   	{ return symbol(sym.PONTO, new String(yytext())); }
<YYINITIAL> ","                   	{ return symbol(sym.VIRGULA, new String(yytext())); }
<YYINITIAL> ":"						{ return symbol(sym.DOISPONTOS, new String(yytext())); }
<YYINITIAL> "::"					{ return symbol(sym.DOISPONTOSDUPLO, new String(yytext())); }
<YYINITIAL> ">"						{ return symbol(sym.MAIOR, new String(yytext())); }
<YYINITIAL> ">="					{ return symbol(sym.MAIORIGUAL, new String(yytext())); }
<YYINITIAL> "<"						{ return symbol(sym.MENOR, new String(yytext())); }
<YYINITIAL> "<="					{ return symbol(sym.MENORIGUAL, new String(yytext())); }
<YYINITIAL> "="						{ return symbol(sym.IGUAL, new String(yytext())); }
<YYINITIAL> "<>"					{ return symbol(sym.DIFERENTE, new String(yytext())); }
<YYINITIAL> "and"					{ return symbol(sym.AND, new String(yytext())); }
<YYINITIAL> "or"					{ return symbol(sym.OR, new String(yytext())); }
<YYINITIAL> "xor"					{ return symbol(sym.XOR, new String(yytext())); }
<YYINITIAL> "not"					{ return symbol(sym.NOT, new String(yytext())); }
<YYINITIAL> "implies"				{ return symbol(sym.IMPLIES, new String(yytext())); }
<YYINITIAL> "pre"					{ return symbol(sym.PRE, new String(yytext())); }
<YYINITIAL> "post"					{ return symbol(sym.POST, new String(yytext())); }
<YYINITIAL> "inv"					{ return symbol(sym.INV, new String(yytext())); }
<YYINITIAL> "context"				{ return symbol(sym.CONTEXT, new String(yytext())); }
<YYINITIAL> "@pre"				    { return symbol(sym.APRE, new String(yytext())); }
<YYINITIAL> "self"					{ return symbol(sym.SELF, new String(yytext())); }
<YYINITIAL> "result"				{ return symbol(sym.RESULT, new String(yytext())); }
<YYINITIAL> "if"					{ return symbol(sym.IF, new String(yytext())); }
<YYINITIAL> "then"					{ return symbol(sym.THEN, new String(yytext())); }
<YYINITIAL> "else"					{ return symbol(sym.ELSE, new String(yytext())); }
<YYINITIAL> "endif"					{ return symbol(sym.ENDIF, new String(yytext())); }
<YYINITIAL> "->"					{ return symbol(sym.ACESSOCOLECOES, new String(yytext())); }
<YYINITIAL> {boolean}        		{ return symbol(sym.BOOL, new Boolean(yytext())); }
<YYINITIAL> {inteiro}        		{ return symbol(sym.INTEGER, new Integer(yytext())); }
<YYINITIAL> {pontoFlututante}    	{ return symbol(sym.REAL, new Double(yytext())); }
<YYINITIAL> {colecao}	            { return symbol(sym.COLECAO, yytext()); }
<YYINITIAL> {id}					{ return symbol(sym.ID, yytext()); }
<YYINITIAL> "+"						{ return symbol(sym.MAIS); }
<YYINITIAL> "-"						{ return symbol(sym.MENOS); }
<YYINITIAL> "*"						{ return symbol(sym.VEZES); }
<YYINITIAL> "/"						{ return symbol(sym.DIVISAO); }
<YYINITIAL> {delimString}			{ 
							  	  	  stringBuilder = new StringBuilder();
							  	  	  yybegin(STRING);
									}
									
<STRING> "\\\\"						{ stringBuilder.append("\\"); }
<STRING> "\\\'"						{ stringBuilder.append("'"); }
<STRING> [^\\\'] 					{ stringBuilder.append(yytext()); }
<STRING> <<EOF>>					{ error("String incompleta!"); }
<STRING> {delimString}				{ 
    						  	 	  yybegin(YYINITIAL);
    					      	  	  return symbol(sym.STRING, stringBuilder.toString());
    								}

.|\n								{ error("Padrao desconhecido: <" + yytext() + "> na linha " + (yyline+1) + " e coluna " + (yycolumn+1) + "!"); }