package core.modeling.metamodel;

public enum TipoPrimitivoEnum {
	INTEGER("Integer"), NUMBER("Number"), REAL("Real"), STRING("String"), BOOLEAN("Boolean"), OCL_TYPE("OclType");
	
	private String nome;
	
	TipoPrimitivoEnum(String nome) {
		this.nome = nome;
	}
	
	public String getNome() {
		return nome;
	}
}
