package core.modeling.metamodel;

import java.util.List;

import org.jdom.Attribute;
import org.jdom.Element;

import core.modeling.ConstantesModelo;
import core.util.Util;

public class Atributo extends Elemento {
	private boolean estatico;
	private VisibilidadeKind visibilidade;
	private String nome;
	private Classe tipo;

	public Atributo(String nome, Classe tipo, VisibilidadeKind visibilidade) {
		this.nome = nome;
		this.tipo = tipo;
		this.visibilidade = visibilidade;
	}

	public Atributo(Element elemento, List<Classe> classes)
			throws ModelingException {
		super(elemento);

		Attribute attrName = elemento.getAttribute("name");
		Attribute attrVis = elemento.getAttribute("visibility");
		Attribute attrType = elemento.getAttribute("type");

		if (attrName != null) {
			this.setNome(attrName.getValue());
		}
		this.setVisibilidade(attrVis.getValue());
		this.parse(attrType, classes);

		if (attrName != null && !attrName.getValue().trim().isEmpty()) {
			this.setNome(attrName.getValue());
		} else {
			this.setNome(Util.getNomeAtributoPadraoUML(this.getTipo()));
		}
	}

	public boolean isEstatico() {
		return estatico;
	}

	public void setEstatico(boolean estatico) {
		this.estatico = estatico;
	}

	public void setVisibilidade(String visibilidade) {
		this.visibilidade = VisibilidadeKind.getByDescricao(visibilidade);
	}

	public VisibilidadeKind getVisibilidade() {
		return visibilidade;
	}

	public void setTipo(Classe tipo) {
		this.tipo = tipo;
	}

	public Classe getTipo() {
		return tipo;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getNome() {
		return nome;
	}
	
	@SuppressWarnings("unchecked")
	private void parse(Attribute attrType, List<Classe> classes)
			throws ModelingException {
		if (attrType == null) {
			String value = "";

			// Tipo primitivo
			Element child = element.getChild("type");
			
			List<Element> childExtension = child.getChildren();			
			List<Element> childrenReferenceExtension = childExtension.get(0).getChildren();
			Element referenceExtension = childrenReferenceExtension.get(0);
			Attribute type = referenceExtension.getAttribute("referentPath");						
			String typeValue = type.getValue();
			int index = typeValue.lastIndexOf(":");
			value = typeValue.substring(index + 1, typeValue.length());
			
			if(value.toUpperCase().equals("DOUBLE") || value.toUpperCase().equals("FLOAT")){
				value = "Real";				
			}
			
			Classe classe = Util.getClasseById(classes, value);
			this.setTipo(classe);
		} else {
			// Tipo do Modelo
			Classe classe = Util.getClasseById(classes, attrType.getValue());

			if (classe != null) {
				// Elemento existe no conjunto de classes do modelo

				this.setTipo(classe);

				Element lowerValue = element
						.getChild(ConstantesModelo.LOWER_VALUE);
				Element upperValue = element
						.getChild(ConstantesModelo.UPPER_VALUE);

				if (lowerValue != null) {
					Attribute lowerValueAtt = lowerValue
							.getAttribute(ConstantesModelo.LOWER_VALUE_ATTR);
					if (lowerValueAtt != null) {
						if (lowerValueAtt.getValue().equals(
								ConstantesModelo.MANY)
								|| Integer.valueOf(lowerValueAtt.getValue()) > 1) {
							setTipo(new Colecao(classe));
						}
					}
				}

				if (upperValue != null) {
					Attribute upperValueAtt = upperValue
							.getAttribute(ConstantesModelo.UPPER_VALUE_ATTR);

					if (upperValueAtt != null) {
						if (upperValueAtt.getValue().equals(
								ConstantesModelo.MANY)
								|| Integer.valueOf(upperValueAtt.getValue()) > 1) {
							this.setTipo(new Colecao(classe));
						}
					}
				}
			} else {
				throw new ModelingException("Elemento n�o encontrado.");
			}
		}
	}
}