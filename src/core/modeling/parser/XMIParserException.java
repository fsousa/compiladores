package core.modeling.parser;

public class XMIParserException extends Exception {
	private static final long serialVersionUID = 1L;

	public XMIParserException(String msg, Exception e) {
		super(msg, e);
	}

	public XMIParserException(Exception e) {
		super(e);
	}

	public XMIParserException(String msg) {
		super(msg);
	}
}
