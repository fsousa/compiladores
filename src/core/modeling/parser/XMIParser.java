package core.modeling.parser;

import core.modeling.GerenciadorEntidades;
import core.modeling.metamodel.Atributo;
import core.modeling.metamodel.Classe;
import core.modeling.metamodel.Metodo;
import core.modeling.metamodel.Parametro;

public class XMIParser implements XMIParserIF {
	private static final String FILE_SEPARATOR = System
			.getProperty("file.separator");

	private static final boolean DEBUG = true;

	private static XMIParser instance;

	private String xmiFileName = "src" + FILE_SEPARATOR + "Test" + FILE_SEPARATOR
			+ "xmi_projeto.xml";

	private XMIParser() {
	}

	public static XMIParser getInstance() {
		if (instance == null) {
			instance = new XMIParser();
		}
		return instance;
	}

	public void parse() throws XMIParserException {
		GerenciadorEntidades gerenciadorEntidades = GerenciadorEntidades
				.getInstance();

		try {
			gerenciadorEntidades.read(getXmiFileName());

			debug("EXECUTANDO PARSE DO XMI");

			for (Classe classe : gerenciadorEntidades.getPacotes()
					.getAllClasses()) {
				debug("***************************************");
				debug("Classe: " + classe.getNome());
				debug("=> Atributos: ");

				for (Atributo atributo : classe.getAtributos()) {
					debug(" - " + atributo.getNome() + ": "
							+ atributo.getTipo().getNome() + ". Visibilidade: "
							+ atributo.getVisibilidade());
				}

				debug("=> Metodos: ");

				for (Metodo metodo : classe.getMetodos()) {
					debug(" - " + metodo.getNome());
					debug("    + Visibilidade: " + metodo.getVisibilidade());
					debug("    + Parametros: ");

					for (Parametro p : metodo.getParametros()) {
						debug("          > Nome: " + p.getNome() + " - "
								+ p.getTipo().getNome());
					}

					if (metodo.getTipoRetorno() != null) {
						debug("    + Tipos retorno: "
								+ metodo.getTipoRetorno().getNome());
					}
				}

				if (classe.getClasseMae() != null) {
					debug("Classe Mae: " + classe.getClasseMae().getNome());
				}
			}

			debug("***************************************");
		} catch (Exception e) {
			throw new XMIParserException("Falha na leitura do XML", e);
		}
	}

	private void debug(String s) {
		if (DEBUG) {
			System.out.println(s);
		}
	}

	public void setXmiFileName(String xmiFileName) {
		this.xmiFileName = xmiFileName;
	}

	public String getXmiFileName() {
		return xmiFileName;
	}

	public static void main(String[] args) throws XMIParserException {
		getInstance().parse();
		/*System.out.println(GerenciadorEntidades.getInstance()
				.getClasse("Transacao").generateJavaCode());*/
	}
}
