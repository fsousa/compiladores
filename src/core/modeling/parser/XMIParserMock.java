package core.modeling.parser;

import core.modeling.GerenciadorEntidades;
import core.modeling.metamodel.Classe;
import core.modeling.metamodel.Metodo;
import core.modeling.metamodel.ModelingException;

public class XMIParserMock implements XMIParserIF {
	private static XMIParserMock instance;

	private XMIParserMock() {
	}

	public static XMIParserMock getInstance() {
		if (instance == null) {
			instance = new XMIParserMock();
		}
		return instance;
	}

	public void parse() {
		try {
			GerenciadorEntidades.getInstance().read();
			GerenciadorEntidades.getInstance().addClasse("classe");

			Classe classe = GerenciadorEntidades.getInstance().getClasse(
					"classe");
			Metodo metodo = new Metodo("metodo");
			metodo.setTipoRetorno(new Classe("Integer"));
			
			classe.addMetodo(metodo);
		} catch (ModelingException e) {
			e.printStackTrace();
		}
	}
}
