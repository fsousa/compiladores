package core.modeling;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import core.modeling.metamodel.Classe;
import core.modeling.metamodel.ModelingException;
import core.modeling.metamodel.Pacote;

public class GerenciadorPacotes {
	private HashMap<String, Pacote> pacotes;

	public GerenciadorPacotes() {
		pacotes = new HashMap<String, Pacote>();
		pacotes.put(ConstantesModelo.PACOTE_PADRAO, new Pacote(
				ConstantesModelo.PACOTE_PADRAO));
	}

	public void addPacote(Pacote p) throws ModelingException {
		if (pacotes.containsKey(p.getNome())) {
			throw new ModelingException("Ja existe pacote " + p.getNome() + "!");
		}
		pacotes.put(p.getNome(), p);
	}

	public Pacote getPacote(String nome) {
		return pacotes.get(nome);
	}

	public Pacote getPacotePadrao() {
		return pacotes.get(ConstantesModelo.PACOTE_PADRAO);
	}

	public List<Classe> getAllClasses() {
		List<Classe> classes = new ArrayList<Classe>();

		ArrayList<Pacote> packages = new ArrayList<Pacote>(pacotes.values());
		for (Pacote pacote : packages) {
			classes.addAll(pacote.getClasses());
			classes.addAll(pacote.getEnumerations());
		}

		return classes;
	}
}