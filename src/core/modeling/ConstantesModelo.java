package core.modeling;

public class ConstantesModelo {
	public static final String LINE_SEPARATOR = System.getProperty("line.separator");
	public static final String PACOTE_PADRAO = "padrao";
	public static final String UML_MODEL = "Model";
	public static final String UML_CLASS = "uml:Class";
	public static final String UML_ENUM = "uml:Enumeration";
	public static final String PACKAGED_ELEMENT = "packagedElement";
	public static final String CONTEUDO_COLLECTION_PATTERN = "<[a-zA-Z]+>";
	public static final String COLLECTION_PATTERN = "(Set|OrderedSet|Bag|Sequence)"
			+ CONTEUDO_COLLECTION_PATTERN;
	public static final String LOWER_VALUE = "lowerValue";
	public static final String LOWER_VALUE_ATTR = "value";
	public static final String UPPER_VALUE = "upperValue";
	public static final String UPPER_VALUE_ATTR = "value";
	public static final String MANY = "*";
}
