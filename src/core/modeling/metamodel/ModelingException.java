package core.modeling.metamodel;

public class ModelingException extends Exception {
	private static final long serialVersionUID = 1L;

	public ModelingException(String m){
		super(m);
	}
}
