lang_name "OCL"
automaton_kind "dtran"
bottomup lalr analyzer
error_handling panic_mode { ")", ":" }
conflict shift-reduce : shift
conflict reduce-reduce : precedence


keywords {
  "package",
  "endpackage",
  "context",
  "init",
  "derive",
  "iterate",
  "static",	
  "@",
  "pre",
  "post",
  "body",
  "inv",
  "def",
  "self",
  "null",
  "invalid",
  "let",		
  "in",		
  "if",		
  "then",	
  "else",	
  "endif",
  "implies"
}

terminal DecIntegerLiteral = 0 | [1-9][0-9]*;
terminal RealLiteral = {DecIntegerLiteral}\.{DecIntegerLiteral}([eE][+-]?[0-9]+)?;
terminal Boolean = ("true" | "false");
terminal ID = [A-Za-z_][A-Za-z_0-9]*;
terminal COLLECTIONTYPE = ("Set" | "Bag" | "Sequence" | "Collection" | "OrderedSet");
terminal PRIMITIVETYPE = ({Boolean} | {DecIntegerLiteral} | {RealLiteral} | {ID} | "UnlimitedNatural");
terminal OCLTYPE = ("OclAny" | "OclInvalid" | "OclMessage" | "OclVoid");


terminal LPAREN = "(";    
terminal RPAREN = ")";    
terminal LBRACE = "{";    
terminal RBRACE = "}";    
terminal LBRACK = "[";    
terminal RBRACK = "]";    
terminal SEMICOLON = ";"; 
terminal COMMA = ",";     
terminal DOT = ".";       
  
terminal EQ = "=";        
terminal GT = ">";        
terminal LT = "<";        
terminal COLON = ":";     
terminal LTEQ = "<=";     
terminal GTEQ = ">=";     
terminal NOTEQ = "<>";    
terminal PLUS = "+";      
terminal MINUS = "-";     
terminal MULT = "*";      
terminal DIV = "/";       
  
terminal UPP = "^^";	
terminal UP = "^";		
terminal ACC = "->";	   
terminal PIPE = "|";	
terminal DOUBLEPOINT = "::";
terminal INTER = "?";	
terminal TUPLE = "Tuple";  
terminal AND = "and";
terminal OR = "or";
terminal XOR = "xor";



<packageDeclarationCS> ::= PACKAGE <pathNameCS> <contextDeclCSOPT> ENDPACKAGE  
	| <contextDeclCSOPT>;

<contextDeclCSOPT> ::= | <contextDeclCS> <contextDeclCSOPT>;	

<pathNameCS> ::= <pathNameCS> DOUBLEPOINT <unreservedSimpleNameCS> | <simpleNameCS> ;

<unreservedSimpleNameCS> ::= <simpleNameCS> 
	| <restrictedKeywordCS>;

<simpleNameCS> ::= ID;
	
<restrictedKeywordCS> ::= <CollectionTypeIdentifierCS> 
	| <primitiveTypeCS> 
	| <oclTypeCS> 
	| TUPLE;
	
<CollectionTypeIdentifierCS> ::= COLLECTIONTYPE;
<primitiveTypeCS> ::= PRIMITIVETYPE;
<oclTypeCS> ::= OCLTYPE;

<contextDeclCS> ::= <propertyContextDeclCS> 
	| <classifierContextDeclCS> 
	| <operationContextDeclCS>;
	
<propertyContextDeclCS> ::= CONTEXT <pathNameCS> DOUBLEPOINT <simpleNameCS> COLON <typeCS> <initOrDerValueCS>;

<initOrDerValueCS> ::= <initOrDer> COLON <OclExpressionCS> <initOrDerValueCSOPT>;
<initOrDerValueCSOPT> ::= | <initOrDerValueCS>;

<initOrDer> ::= INIT | DERIVE;

<classifierContextDeclCS> ::= CONTEXT <pathNameCS> <invOrDefCS> 
	| CONTEXT <simpleNameCS> COLON <pathNameCS> <invOrDefCS>;

<invOrDefCS> ::= INV <simpleNameCSOPT> COLON <OclExpressionCS> <invOrDefCSOPT> 
	| staticOPT DEF <simpleNameCSOPT> COLON <defExpressionCS> <invOrDefCSOPT>;
staticOPT ::= | STATIC;
<invOrDefCSOPT> ::= | <invOrDefCS> <invOrDefCSOPT>;
<simpleNameCSOPT> ::= | <simpleNameCS>;

<defExpressionCS> ::= <VariableDeclarationCS> EQ <OclExpressionCS> 
	| <operationCS> EQ <OclExpressionCS>; 

<VariableDeclarationCS> ::= <simpleNameCS> <typeEmpty> <expressionEmpty>;
<typeEmpty> ::= | COLON <typeCS>;
<expressionEmpty> ::= | EQ <OclExpressionCS>;

<operationContextDeclCS> ::= CONTEXT <operationCS> <prePostOrBodyDeclCS>;

<operationCS> ::= <pathNameCS> DOUBLEPOINT <simpleNameCS> LPAREN <parametersCSOPT> RPAREN COLON <typeCSOPT>
	| <simpleNameCS> LPAREN <parametersCSOPT> RPAREN COLON <typeCSOPT>;

<parametersCSOPT> ::= | <parametersCS>;

<parametersCS> ::= <VariableDeclarationCS> <parametersCSOPT2>;

<parametersCSOPT2> ::= COMMA <parametersCS>;

<typeCSOPT> ::= | <typeCS>;

<prePostOrBodyDeclCS> ::= condition <simpleNameCSOPT> COLON <OclExpressionCS> <prePostOrBodyDeclCSOPT>;

condition ::= PRE | POST | BODY;

<prePostOrBodyDeclCSOPT> ::= | <prePostOrBodyDeclCS>;

<OclExpressionCS> ::= <CallExpCS> 
	| <VariableExpCS> 
	| <LiteralExpCS> 
	| <LetExpCS> 
	| <OclMessageExpCS> 
	| <IfExpCS>;

<CallExpCS> ::= <FeatureCallExpCS> 
	| <LoopExpCS>;
	
<LoopExpCS> ::= <IteratorExpCS> | <IterateExpCS>;

<IteratorExpCS> ::= <OclExpressionCS> ACC <simpleNameCS> LPAREN <varDeclar> <OclExpressionCS> RPAREN
	| <OclExpressionCS> DOT <simpleNameCS> LPAREN <argumentsCSOPT> RPAREN
	| <OclExpressionCS> DOT <simpleNameCS>
	| <OclExpressionCS> DOT <simpleNameCS> <argOPT>;
varDeclar ::= | <VariableDeclarationCS> varDeclarOPT PIPE;
varDeclaOPT ::= | COMMA <VariableDeclarationCS>;
argOPT ::= | LBRACK <argumentsCS> RBRACK;

<IterateExpCS> ::= <OclExpressionCS> ACC ITERATE LPAREN <semiVarOPT> <VariableDeclarationCS> PIPE <OclExpressionCS> RPAREN;
<semiVarOPT> ::= <VariableDeclarationCS> SEMICOLON;	

<FeatureCallExpCS> ::= <OperationCallExpCS> 
	| <PropertyCallExpCS> 
	| <NavigationCallExpCS>;

<NavigationCallExpCS> ::= <PropertyCallExpCS> 
	| <AssociationClassCallExpCS>;	

<AssociationClassCallExpCS> ::= <OclExpressionCS> DOT <simpleNameCS> <argBrackOPT> <isMarkedPreCSOPT>
	| <simpleNameCS> <argBrackOPT> <isMarkedPreCSOPT>;
<argBrackOPT> ::= LBRACK <argumentsCS> RBRACK;

<PropertyCallExpCS> ::= <OclExpressionCS> DOT <simpleNameCS> <isMarkedPreCSOPT>
	| <simpleNameCS> <isMarkedPreCSOPT> 
	| <pathNameCS> 
	| <OclExpressionCS> DOT <pathNameCS> DOUBLEPOINT <simpleNameCS> <isMarkedPreCSOPT>;

<isMarkedPreCSOPT> ::= | <isMarkedPreCS>;

<OperationCallExpCS> ::= <OclExpressionCS> <simpleNameCS> <OclExpressionCS>
	| <OclExpressionCS> ACC <simpleNameCS> LPAREN <argumentsCSOPT> RPAREN
	| <OclExpressionCS> DOT <simpleNameCS> LPAREN <argumentsCSOPT> RPAREN
	| <simpleNameCS> LPAREN <argumentsCSOPT> RPAREN
	| <OclExpressionCS> DOT <simpleNameCS> <isMarkedPreCS> LPAREN <argumentsCSOPT> RPAREN
	| <simpleNameCS> <isMarkedPreCS> LPAREN <argumentsCSOPT> RPAREN;
	
<isMarkedPreCS> ::= ATPRE;
<argumentsCSOPT> ::= | <argumentsCS>;

<argumentsCS> ::= <OclExpressionCS> <argumentsCSLine>;
<argumentsCSLine> ::= | COMMA <argumentsCS>;

<VariableExpCS> ::= <simpleNameCS> | SELF;
	
<LiteralExpCS> ::= <EnumLiteralExpCS> 
	| <CollectionLiteralExpCS> 
	| <TupleLiteralExpCS> 
	| <PrimitiveLiteralExpCS> 
	| <TypeLiteralExpCS>;

<LetExpCS> ::= LET <VariableDeclarationCS> <LetExpSubCS>;
<LetExpSubCS> ::= COMMA <VariableDeclarationCS> <LetExpSubCS> 
	| IN <OclExpressionCS>;

<OclMessageExpCS> ::= <OclExpressionCS> UPP <simpleNameCS> LPAREN <OclMessageArgumentsCSOPT> RPAREN 
	| <OclExpressionCS> UP <simpleNameCS> LPAREN <OclMessageArgumentsCSOPT> RPAREN;
<OclMessageArgumentsCSOPT> ::= | <OclMessageArgumentsCS>;

<OclMessageArgumentsCS> ::= <OclMessageArgCS> <oclCommaOpt>;
<oclCommaOpt> ::= | COMMA <OclMessageArgumentsCS>; 

<OclMessageArgCS> ::= INTER <typeCSoptColon> | <OclExpressionCS>;
<typeCSoptColon> ::= | COLON <typeCS>;

<IfExpCS> ::= IF <OclExpressionCS> THEN <OclExpressionCS> ELSE <OclExpressionCS> ENDIF;

<EnumLiteralExpCS> ::= <pathNameCS> DOUBLEPOINT <simpleNameCS>;

<CollectionLiteralExpCS> ::= <CollectionTypeIdentifierCS> LBRACE <CollectionLiteralPartsCSOPT> RBRACE;

<CollectionLiteralPartsCSOPT> ::= | <CollectionLiteralPartsCS>;

<CollectionLiteralPartsCS> ::= <CollectionLiteralPartCS> <CollectionLiteralPartsCSOPTC>;

<CollectionLiteralPartsCSOPTC> ::= | COMMA <CollectionLiteralPartsCS>;

<CollectionLiteralPartCS> ::= <CollectionRangeCS> | <OclExpressionCS>;

<CollectionRangeCS> ::= <OclExpressionCS> COMMA <OclExpressionCS>;

<TupleLiteralExpCS> ::= TUPLE LBRACE <variableDeclarationListCS> RBRACE;

<variableDeclarationListCS> ::= <VariableDeclarationCS> <VariableDeclarationCSLOPT>;
<VariableDeclarationCSLOPT> ::= | COMMA <variableDeclarationListCS>;

<PrimitiveLiteralExpCS> ::= <IntegerLiteralExpCS> 
	| <RealLiteralExpCS> 
	| <StringLiteralExpCS> 
	| <BooleanLiteralExpCS> 
	| <UnlimitedNaturalLiteralExpCS> 
	| <NullLiteralExpCS> 
	| <InvalidLiteralExpCS>;

<IntegerLiteralExpCS> ::= INT;
<RealLiteralExpCS> ::= FLOAT;
<StringLiteralExpCS> ::= STRING_LITERAL;
<BooleanLiteralExpCS> ::= BOOLEAN;
<UnlimitedNaturalLiteralExpCS> ::= MULT; 
<NullLiteralExpCS> ::= NULL;
<InvalidLiteralExpCS> ::= INVALID;

<TypeLiteralExpCS> ::= <typeCS>;

<typeCS> ::=  <pathNameCS> 
	| <collectionTypeCS> 
	| <tupleTypeCS> 
	| <primitiveTypeCS> 
	| <oclTypeCS>;
	
<tupleTypeCS> ::= TUPLE LPAREN <variableDeclarationListCSOPT> RPAREN;
<variableDeclarationListCSOPT> ::= | <variableDeclarationListCS>;

<collectionTypeCS> ::= <CollectionTypeIdentifierCS> LPAREN <typeCS> RPAREN;


