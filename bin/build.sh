#! /bin/bash

##Var

JFLEXF="OCL.jflex"
CUPF="OCL.cup"

## Limpa
rm -rf *.ja*

### Compila
##Jflex

jflex -v $JFLEXF

##Cup
java -jar lib/java-cup-11a.jar -expect 3000 $CUPF

## Testa
#bash runTest.sh
