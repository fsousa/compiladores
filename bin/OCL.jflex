import java_cup.runtime.*;

%%

%public
%class OCLex
%unicode
%line
%column
%cup


%{
  StringBuffer string = new StringBuffer();
  
  private Symbol symbol(int type) {
		return new Symbol(type, yyline, yycolumn);
	}
	private Symbol symbol(int type, Object value) {
		return new Symbol(type, yyline, yycolumn, value);
	}
	private void error(String message) {
    	System.out.println("Error at line "+(yyline+1)+", column "+(yycolumn+1)+" : "+message);
    }

  public String current_lexeme(){
    int l = yyline+1;
    int c = yycolumn+1;
    return " (line: "+l+" , column: "+c+" , lexeme: '"+yytext()+"')";
  }

  /** 
   * assumes correct representation of a long value for 
   * specified radix in scanner buffer from <code>start</code> 
   * to <code>end</code> 
   */
  private long parseLong(int start, int end, int radix) {
    long result = 0;
    long digit;

    for (int i = start; i < end; i++) {
      digit  = Character.digit(yycharat(i),radix);
      result*= radix;
      result+= digit;
    }

    return result;
  }
%}


/* main character classes */
LineTerminator = \r|\n|\r\n
InputCharacter = [^\r\n]

WhiteSpace = {LineTerminator} | [ \t\f]

/* comments */

Comment = "--" ([^\n\r])*[\n\r]
EndOfLineComment = "//" {InputCharacter}* {LineTerminator}?

/* identifiers */
Identifier = [A-Za-z_][A-Za-z_0-9]*

/* integer literals */
DecIntegerLiteral = 0 | [1-9][0-9]*
RealLiteral = {DecIntegerLiteral}\.{DecIntegerLiteral}([eE][+-]?[0-9]+)?

/* string and character literals */
StringCharacter = [^\r\n\"\\]
SingleCharacter = [^\r\n\'\\]

Boolean = ("true" | "false")
CollectionType = ("Set" | "Bag" | "Sequence" | "Collection" | "OrderedSet")
PrimitiveType = ({Boolean} | {DecIntegerLiteral} | {RealLiteral} | {Identifier} | "UnlimitedNatural")
OclType = ("OclAny" | "OclInvalid" | "OclMessage" | "OclVoid")

%state STRING, CHARLITERAL

%%

<YYINITIAL> {

  /* keywords */
  "package"               	 { return symbol(sym.PACKAGE); }
  "endpackage"            	 { return symbol(sym.ENDPACKAGE); }
  "context"					 { return symbol(sym.CONTEXT); }
  "init"					 { return symbol(sym.INIT);}
  "derive"				 	 { return symbol(sym.DERIVE);}
  "iterate"				 	 { return symbol(sym.ITERATE);}
  "static"					 { return symbol(sym.STATIC);}
  "@pre"					 { return symbol(sym.ATPRE);}
  "pre"					 	 { return symbol(sym.PRE);}
  "post"					 { return symbol(sym.POST);}
  "body"					 { return symbol(sym.BODY);}
  "inv"					 	 { return symbol(sym.INV);}
  "def"					 	 { return symbol(sym.DEF);}
  "self"					 { return symbol(sym.SELF);}
  "null"					 { return symbol(sym.NULL);}
  "invalid"				 	 { return symbol(sym.INVALID);}
  "let"					 	 { return symbol(sym.LET);}
  "in"					 	 { return symbol(sym.IN);}
  "if"					 	 { return symbol(sym.IF);}
  "then"					 { return symbol(sym.THEN);}
  "else"					 { return symbol(sym.ELSE);}
  "endif"					 { return symbol(sym.ENDIF);}
  "implies"				     { return symbol(sym.IMPLIES);}

  /* separators */
  "("                            { return symbol(sym.LPAREN); }
  ")"                            { return symbol(sym.RPAREN); }
  "{"                            { return symbol(sym.LBRACE); }
  "}"                            { return symbol(sym.RBRACE); }
  "["                            { return symbol(sym.LBRACK); }
  "]"                            { return symbol(sym.RBRACK); }
  ";"                            { return symbol(sym.SEMICOLON); }
  ","                            { return symbol(sym.COMMA); }
  "."                            { return symbol(sym.DOT); }
  
  /* operators */
  "="                            { return symbol(sym.EQ); }
  ">"                            { return symbol(sym.GT); }
  "<"                            { return symbol(sym.LT); }
  ":"                            { return symbol(sym.COLON); }
  "<="                           { return symbol(sym.LTEQ); }
  ">="                           { return symbol(sym.GTEQ); }
  "<>"                           { return symbol(sym.NOTEQ); }
  "+"                            { return symbol(sym.PLUS); }
  "-"                            { return symbol(sym.MINUS); }
  "*"                            { return symbol(sym.MULT); }
  "/"                            { return symbol(sym.DIV); }
  
  "^^"							{ return symbol(sym.UPP);}
  "^"							{ return symbol(sym.UP);}
  "->"							{ return symbol(sym.ACC);}   
  "|"							{ return symbol(sym.PIPE);}
  "::"							{ return symbol(sym.DOUBLEPOINT); }
  "?"							{ return symbol(sym.INTER); }
  "Tuple"						{ return symbol(sym.TUPLE); }
  
  "and"							{ return symbol(sym.AND) ;}
  "or"					 		{ return symbol(sym.OR); }
  "xor"					 		{ return symbol(sym.XOR); }


  {Identifier}                  { return symbol(sym.ID, yytext()); } 
  {CollectionType} 		 		{ return symbol(sym.COLLECTIONTYPE); }
  {PrimitiveType} 				{ return symbol(sym.PRIMITIVETYPE, new Integer(yytext())); }
  {OclType} 					{ return symbol(sym.OCLTYPE); }
  {DecIntegerLiteral}           { return symbol(sym.INTEGER_LITERAL, new Integer(yytext())); }
  {RealLiteral}					{ return symbol(sym.FLOAT, new Float(yytext()); }
  {Boolean}						{ return symbol(sym.BOOL, new Boolean(yytext()); }
  
  
  /* string literal */
  \"                             { yybegin(STRING); string.setLength(0); }

  /* character literal */
  \'                             { yybegin(sym.CHARLITERAL); }

  /* numeric literals */
PrimitiveType = ({Boolean} | {DecIntegerLiteral} | {RealLiteral} | {Identifier} | "UnlimitedNatural")
  
  /* comments */
  {Comment}                      { /* ignore */ }

  /* whitespace */
  {WhiteSpace}                   { /* ignore */ }
  
}

<STRING> {
  \"                             { yybegin(YYINITIAL); return symbol(sym.STRING_LITERAL); }
  
  {StringCharacter}+             { string.append( yytext() ); }
  
  /* escape sequences */
  "\\b"                          { string.append( '\b' ); }
  "\\t"                          { string.append( '\t' ); }
  "\\n"                          { string.append( '\n' ); }
  "\\f"                          { string.append( '\f' ); }
  "\\r"                          { string.append( '\r' ); }
  "\\\""                         { string.append( '\"' ); }
  "\\'"                          { string.append( '\'' ); }
  "\\\\"                         { string.append( '\\' ); }

  
  /* error cases */
  \\.                            { throw new RuntimeException("Illegal escape sequence \""+yytext()+"\""); }
  {LineTerminator}               { throw new RuntimeException("Unterminated string at end of line"); }
}
  
  /* error cases */
  {LineTerminator}               { throw new RuntimeException("Unterminated character literal at end of line"); }

/* error fallback */
.|\n             				 {error("Illegal character <"+ yytext()+">");}
<<EOF>>                          { return symbol(sym.EOF); }

